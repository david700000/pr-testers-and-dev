*** Settings ***
Library  SeleniumLibrary    timeout=10s
Resource  ../Resources/Common.robot
Resource  ../Resources/Keywords.robot

*** Variables ***
${browser}  chrome
${url}  https://cvr.inecnigeria.org/pu
@{States}=    23 - KWARA    22 - KOGI    30 - OYO    29 - OSUN
@{LGA}=  01 - ASA  12 - LOKOJA    32 - SAKI WEST    30 - OSOGBO
@{RegistrationArea}=  12 - AFON  10 - EGGAN  10 - SANGOTE/BOODA/BAABO/ILUA  13 - EKETA
@{PollingUnit}=  001 - AFON L.G.E.A.SCH  001 - EGGAN TIFAIN AREA, LGEA SCH. EGGAN  003 - IBARU AREA, OPEN SPACE, IBARU    005 - 44, OLUSEGUN STREET


*** Test Cases ***
Four States in a single testcase
  FOR    ${i}    IN RANGE  4
        Open INEC website
        Select State ${States[${i}]}
        Select LGA ${LGA[${i}]}
        Select RegistrationArea ${RegistrationArea[${i}]}
        Select PollingUnit ${PollingUnit[${i}]}
        Click Submit
        Close INEC website
  END



