*** Settings ***
Library  SeleniumLibrary
*** Keywords ***
Select State ${State}
  
  wait until page contains Element  xpath://*[@id="SearchStateId"]  ${State}
  Select from list by label  xpath://*[@id="SearchStateId"]  ${State}

Select LGA ${LGA}
  
  Wait until page contains Element  xpath://*[@id="SearchLocalGovernmentId"]  ${LGA}
  Select from list by label  xpath://*[@id="SearchLocalGovernmentId"]  ${LGA} 
  
Select RegistrationArea ${RegistrationArea}
  
  Wait until page contains Element  xpath://*[@id="SearchRegistrationAreaId"]  ${RegistrationArea}
  Select from list by label  xpath://*[@id="SearchRegistrationAreaId"]  ${RegistrationArea}

Select PollingUnit ${PollingUnit}
  
  Wait until page contains Element  xpath://*[@id="SearchPollingUnitId"]  ${PollingUnit}
  Select from list by label  xpath://*[@id="SearchPollingUnitId"]  ${PollingUnit}

Click Submit
  wait until page contains Element  xpath://*[@id="SearchIndexForm"]
  Submit form  xpath://*[@id="SearchIndexForm"]