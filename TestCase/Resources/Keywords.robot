*** Settings ***
Library  SeleniumLibrary
*** Keywords ***

Select State ${State}

  Wait Until Page contains Element  xpath://*[@id="SearchStateId"]
  Select From List By Label  xpath://*[@id="SearchStateId"]  ${State}

Select LGA ${LGA}
  Wait Until Page contains Element  xpath://*[@id="SearchLocalGovernmentId"]
  Select from list by label  xpath://*[@id="SearchLocalGovernmentId"]  ${LGA}

Select RegistrationArea ${Reg Area}
  Wait Until Page contains Element  xpath://*[@id="SearchRegistrationAreaId"]
  Select from list by label  xpath://*[@id="SearchRegistrationAreaId"]  ${Reg Area}

Select PollingUnit ${PollingUnit}
  Wait Until Page contains Element  xpath://*[@id="SearchPollingUnitId"]
  Select from list by label  xpath://*[@id="SearchPollingUnitId"]  ${PollingUnit}


Click Submit
  Submit form  xpath://*[@id="SearchIndexForm"]

