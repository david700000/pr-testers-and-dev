# PR-Testers and Dev
This is a script to fill a form with drop down list

There are three files in the folder named TestCase
*COMMON FUNCTIONLITY 
*USER DEFINED KEYWORDS
*Pr.robot

The **CommonFunctionality** and **UserDefined Keywords** is a file under a sub-folder in TestCase named "Resources".
The resouces folder contains the **Commonly used KEYWORDS** and **UserDefined KEYWORDS**.
The following are examples of **CommonFunctionality**

"Open browser"
"Maximize browser"
"Close browser"
These are the most commonly used keywords in robotframework

While the **UserDefined keywords** 
examples:
Page should contain list 
select list by label
unselect list by label e.t.c

The resouces folder helps use to call our keywords in our main robot file 
we can just reference to it as used in the project file.


Our main robot file is Pr.robot
we only need to call out our function keywords from our resources folder 
Resources  ..\ ******\UserDefined keywords
Resources  ..\ ******\CommonFunctionality



